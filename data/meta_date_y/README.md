# Acerca

Acá se tienen los datos ya procesados para determinar cuantas contribuciones
realizó un autor en un año determinado.

`IMPORTANTE`: para estos datos se trabajó específicamente con el atributo que
indica el año de publicación del articulo para un registro determinado, por
lo que se ignoraron los registros que no cuentan con este atributo definido.
