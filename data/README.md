- `sedici_dataset`: Primer versión del dataset, este se obtuvo con la
primer versión del web scrapper, al analizar este dataset se tienen
varias celdas en diferentes registros como nulas.

- `sedici_dataset_v1.1`: Esta versión construye sobre la versión
anterior. En esta versión se disminuyen la cantidad de nulos en gran
medida. Rellenando con información tras realizar un analisis de
contenido faltante. El procedimiento se puede encontrar
[aqui](https://colab.research.google.com/drive/1cxy0wpRT7JkwJMw4J4Moaokcbhns8PkU?usp=sharing).
