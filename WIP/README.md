- [x] Mejorar keywords

  > Es necesario revisar las keywords de los documentos que si cuentan con
  > metadatos, esto por el hecho de que algunas palabras clave están expresadas
  > como una clave compuesta de varias otras claves mediante un separador
  > (ej: "base de datos, ingeniería de software, modelado". _Cuenta como una KW_)

  - [x] Buscar las palabras clave en los artículos con metadatos y
        descomponer las que apliquen al criterio mencionado anteriormente.

- [ ] Explorar datos de los documentos con meta.
  > Doc => sedici_dataset_w_meta.csv
  >
  > Acá se notó que algunos de los títulos/resúmenes estaban incorrectos.
  > Esto ocurrió porque el scrapper no tomaba en cuenta ciertas cuestiones
  > presentes en el HTML de la página del SEDICI.
  - [ ] Ponderar la posibilidad de mejorar el contenido de los documentos con
        problemas
- [ ] Revisar los documentos sin metadatos.
  > sedici_dataset_no_meta.csv
  - [ ] Refactorizar el scrapper para conseguir los datos faltantes.
